# ns-3-fq-comparisons

**Steps to use the script:**
1. Clone the ns-3-dev repository from [GitLab Repo](https://gitlab.com/nsnam/ns-3-dev).
2. Please follow the ns-3 manual or tutorial on how to build the source.
3. Copy the `fq-comparison.cc` file into the `scratch/` folder.
4. To run the file with default argument, please use:
```bash
./waf --run scratch/fq-comparison.cc
```
5. Copy all the `gnu*` folders into the `results/` folder formed in the ns-3-dev.
6. Copy the `plot.py` into `results/` folder.
7. Change the directory to `results/` and run `python3 plot.py`.
8. All the PNG plots will be generated in the same folder.


## Different cases:

1. FQ-CoDel with ECN
```bash
./waf --run "scratch/fq-comparison.cc --linkDelay=0.25ms --bottleneckDelay=1.5ms --bottleneckQueueType=fqCodel --n0TcpType=cubic --bottleneckRate=1Gbps --linkRate=100Gbps --stopTime=40s --useEcn=true"
```

2. FQ-CoDel with ECN disabled
```bash
./waf --run "scratch/fq-comparison.cc --linkDelay=0.25ms --bottleneckDelay=1.5ms --bottleneckQueueType=fqCodel --n0TcpType=cubic --bottleneckRate=1Gbps --linkRate=100Gbps --stopTime=40s --useEcn=false"
```

3. FQ-Pie with ECN
```bash
./waf --run "scratch/fq-comparison.cc --linkDelay=0.25ms --bottleneckDelay=1.5ms --bottleneckQueueType=fqPie --n0TcpType=cubic --bottleneckRate=1Gbps --linkRate=100Gbps --stopTime=40s --useEcn=true"
```

4. FQ-Pie with ECN disabled
```bash
./waf --run "scratch/fq-comparison.cc --linkDelay=0.25ms --bottleneckDelay=1.5ms --bottleneckQueueType=fqPie --n0TcpType=cubic --bottleneckRate=1Gbps --linkRate=100Gbps --stopTime=40s --useEcn=false"
```