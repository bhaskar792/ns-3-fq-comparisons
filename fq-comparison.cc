/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2020 NITK Surathkal
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Bhaskar Kataria <bhaskar.k7920@gmail.com>
 *          Tom Henderson <tomhend@u.washington.edu>
 *          Mohit P. Tahiliani <tahiliani@nitk.edu.in>
 *          Vivek Jain <jain.vivek.anand@gmail.com>
 *          Ankit Deepak <adadeepak8@gmail.com>
 * This script is written using Tom Henderson's L4S evaluation available at https://gitlab.com/tomhend/modules/l4s-evaluation
 */

/**
 * Program options
 * ---------------
 *    --n0TcpType:           First TCP type (bic, dctcp, or reno) [bic]
 *    --n1TcpType:           Second TCP type (cubic, dctcp, or reno) []
 *    --bottleneckQueueType: M3 queue type (fq or codel) [fq]
 *    --baseRtt:             base RTT [80ms]
 *    --useCeThreshold:      use CE threshold [false]
 *    --ceThreshold:         CE threshold [1ms]
 *    --bottleneckRate       data rate of bottleneck [100Mbps]
 *    --linkrate:            data rate of edge link to bottleneck link [1Gbps]
 *    --stopTime:            simulation stop time [70s]
 *    --enablePcap:          enable Pcap [false]
 *    (additional arguments to control trace names)
 */

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/traffic-control-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("FqCoDelL4SExample");

uint32_t checkTimes;
double avgQueueDiscSize;

uint32_t g_n0BytesReceived = 0;
uint32_t g_n1BytesReceived = 0;
uint32_t g_marksObserved = 0;
uint32_t g_dropsObserved = 0;
uint32_t g_n0BytesSent = 0;
uint64_t bottleneckRateNumber = 0;

Time lastTx = Seconds (0);

void
TraceN0Cwnd (std::ofstream* ofStream, uint32_t oldCwnd, uint32_t newCwnd)
{
  // TCP segment size is configured below to be 1448 bytes
  // so that we can report cwnd in units of segments
  *ofStream << Simulator::Now ().GetSeconds () << " " << static_cast<double> (newCwnd) / 1448 << std::endl;
}

void
TraceN0Rtt (std::ofstream* ofStream, Time oldRtt, Time newRtt)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << newRtt.GetSeconds () * 1000 << std::endl;
}

void
TracePingRtt (std::ofstream* ofStream, Time rtt)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << rtt.GetSeconds () * 1000 << std::endl;
}

void
TraceN0Rx (Ptr<const Packet> packet, const Address &address)
{
  g_n0BytesReceived += packet->GetSize ();
}

void
TraceDrop (std::ofstream* ofStream, Ptr<const QueueDiscItem> item)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << std::hex << item->Hash () << std::endl;
  g_dropsObserved++;
}

void
TraceMark (std::ofstream* ofStream, Ptr<const QueueDiscItem> item, const char* reason)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << std::hex << item->Hash () << std::endl;
  g_marksObserved++;
}

void
TraceQueueLength  (std::ofstream* ofStream, DataRate linkRate, uint32_t oldVal, uint32_t newVal)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << newVal << std::endl;
}

void
TraceDropsFrequency (std::ofstream* ofStream, Time dropsSamplingInterval)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << g_dropsObserved << std::endl;
  // g_dropsObserved = 0;
  Simulator::Schedule (dropsSamplingInterval, &TraceDropsFrequency, ofStream, dropsSamplingInterval);
}

void
TraceMarksFrequency (std::ofstream* ofStream, Time marksSamplingInterval)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << g_marksObserved << std::endl;
  // g_marksObserved = 0;
  Simulator::Schedule (marksSamplingInterval, &TraceMarksFrequency, ofStream, marksSamplingInterval);
}

void
TraceN0Throughput (std::ofstream* ofStream, Time throughputInterval)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << g_n0BytesReceived * 8 / throughputInterval.GetSeconds () / 1e6 << std::endl;
  g_n0BytesReceived = 0;
  Simulator::Schedule (throughputInterval, &TraceN0Throughput, ofStream, throughputInterval);
}

void
ScheduleN0TcpCwndTraceConnection (std::ofstream* ofStream)
{
  Config::ConnectWithoutContext ("/NodeList/1/$ns3::TcpL4Protocol/SocketList/0/CongestionWindow", MakeBoundCallback (&TraceN0Cwnd, ofStream));
}

void
ScheduleN0TcpRttTraceConnection (std::ofstream* ofStream)
{
  Config::ConnectWithoutContext ("/NodeList/1/$ns3::TcpL4Protocol/SocketList/0/RTT", MakeBoundCallback (&TraceN0Rtt, ofStream));
}

void
ScheduleN0PacketSinkConnection (void)
{
  Config::ConnectWithoutContext ("/NodeList/6/ApplicationList/*/$ns3::PacketSink/Rx", MakeCallback (&TraceN0Rx));
}

void
TraceTx (std::ofstream* ofStream, Ptr<const Packet> packet)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << g_marksObserved << std::endl;
  g_n0BytesSent += packet->GetSize ();
}

void
TraceLinkUtilization (std::ofstream* ofStream, Time interval)
{
  *ofStream << Simulator::Now ().GetSeconds () << " " << (((g_n0BytesSent * 8 / interval.GetSeconds ()) / 1e6) / bottleneckRateNumber) * 100 << std::endl;
  g_n0BytesSent = 0;
  Simulator::Schedule (interval, &TraceLinkUtilization, ofStream, interval);
}


static void
PacketDequeue (std::ofstream* n0OfStream, Ptr<QueueDiscItem const> item)
{
  Ptr<Packet> p = item->GetPacket ();
  Ptr<const Ipv4QueueDiscItem> iqdi = Ptr<const Ipv4QueueDiscItem> (dynamic_cast<const Ipv4QueueDiscItem *> (PeekPointer (item)));
  Ipv4Address address = iqdi->GetHeader ().GetDestination ();
  Time qDelay = Simulator::Now () - item->GetTimeStamp ();
  if (address == "192.168.2.2")
    {
      *n0OfStream << Simulator::Now ().GetSeconds () << " " << qDelay.GetMicroSeconds () / 1000.0 << std::endl;
    }
}

int
main (int argc, char *argv[])
{
  ////////////////////////////////////////////////////////////
  // variables not configured at command line               //
  ////////////////////////////////////////////////////////////
  uint32_t pingSize = 100; // bytes
  Time pingInterval = MilliSeconds (100);
  Time marksSamplingInterval = MilliSeconds (100);
  Time throughputSamplingInterval = MilliSeconds (200);

  ////////////////////////////////////////////////////////////
  // variables configured at command line                   //
  ////////////////////////////////////////////////////////////
  Time stopTime = Seconds (70);
  Time baseRtt = MilliSeconds (80);
  Time linkDelay = MilliSeconds (10);
  Time bottleneckDelay = MilliSeconds (20);
  bool enablePcap = false;
  bool useCeThreshold = false;
  Time ceThreshold = MilliSeconds (1);
  std::string n0TcpType = "cubic";
  bool useEcn = true;
  std::string queueType = "fqPie";
  std::string linkRate = "100Mbps";
  std::string bottleneckRate = "10Mbps";
  uint32_t tcpStreams = 1;

  ////////////////////////////////////////////////////////////
  // Override ns-3 defaults                                 //
  ////////////////////////////////////////////////////////////
  Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (1448));
  // Increase default buffer sizes to improve throughput over long delay paths
  Config::SetDefault ("ns3::TcpSocket::SndBufSize",UintegerValue (8192000));
  Config::SetDefault ("ns3::TcpSocket::RcvBufSize",UintegerValue (8192000));
  Config::SetDefault ("ns3::TcpSocket::InitialCwnd", UintegerValue (10));
  Config::SetDefault ("ns3::TcpL4Protocol::RecoveryType", TypeIdValue (TcpPrrRecovery::GetTypeId ()));
  Config::SetDefault ("ns3::FqPieQueueDisc::QueueDelayReference", TimeValue (Seconds (0.005)));
  Config::SetDefault ("ns3::FqPieQueueDisc::MaxBurstAllowance", TimeValue (Seconds (0.1)));

  ////////////////////////////////////////////////////////////
  // command-line argument parsing                          //
  ////////////////////////////////////////////////////////////
/*
  TODO:
    add queues for bottleneck: Done
    add rtt for link and bottleneck: Done
    add TCP Cubic : Done
    add multiple application to one node, use 1 node for TCP one for UDP
*/
  CommandLine cmd;
  cmd.AddValue ("n0TcpType", "n0 TCP type (bic, dctcp, or reno)", n0TcpType);
  cmd.AddValue ("bottleneckQueueType", "n2 queue type (fqCodel/fqPie/fqCobalt)", queueType);
  cmd.AddValue ("baseRtt", "base RTT", baseRtt);
  cmd.AddValue ("linkDelay", "Link Delay", linkDelay);
  cmd.AddValue ("bottleneckDelay", "Bottleneck Delay", bottleneckDelay);
  cmd.AddValue ("useCeThreshold", "use CE Threshold", useCeThreshold);
  cmd.AddValue ("useEcn", "use ECN", useEcn);
  cmd.AddValue ("ceThreshold", "CoDel CE threshold", ceThreshold);
  cmd.AddValue ("bottleneckRate", "data rate of bottleneck", bottleneckRate);
  cmd.AddValue ("linkRate", "data rate of edge link", linkRate);
  cmd.AddValue ("stopTime", "simulation stop time", stopTime);
  cmd.AddValue ("enablePcap", "enable Pcap", enablePcap);
  cmd.AddValue ("tcpStreams", "Number of TCP flows", tcpStreams);
  cmd.Parse (argc, argv);

  std::string dir = "results/" + queueType + (useEcn ? "_Ecn" : "_no_Ecn") + "/";
  std::string dirToSave = "mkdir -p " + dir;
  if (system (dirToSave.c_str ()) == -1)
    {
      exit (1);
    }

  std::string pingTraceFile = dir + "ping.dat";
  std::string n0TcpRttTraceFile = dir + "n0-tcp-rtt.dat";
  std::string n0TcpCwndTraceFile = dir + "n0-tcp-cwnd.dat";
  std::string n0TcpThroughputTraceFile = dir + "n0-tcp-throughput.dat";
  std::string dropTraceFile = dir + "drops.dat";
  std::string dropsFrequencyTraceFile = dir + "drops-frequency.dat";
  std::string lengthTraceFile = dir + "length.dat";
  std::string markTraceFile = dir + "mark.dat";
  std::string marksFrequencyTraceFile = dir + "marks-frequency.dat";
  std::string queueDelayN0TraceFile = dir + "queue-delay-n0.dat";
  std::string linkUtilizationFile = dir + "link-utilization.dat";

  cmd.AddValue ("pingTraceFile", "filename for ping tracing", pingTraceFile);
  cmd.AddValue ("n0TcpRttTraceFile", "filename for n0 rtt tracing", n0TcpRttTraceFile);
  cmd.AddValue ("n0TcpCwndTraceFile", "filename for n0 cwnd tracing", n0TcpCwndTraceFile);
  cmd.AddValue ("n0TcpThroughputTraceFile", "filename for n0 throughput tracing", n0TcpThroughputTraceFile);
  cmd.AddValue ("dropTraceFile", "filename for n2 drops tracing", dropTraceFile);
  cmd.AddValue ("dropsFrequencyTraceFile", "filename for n2 drop frequency tracing", dropsFrequencyTraceFile);
  cmd.AddValue ("lengthTraceFile", "filename for n2 queue length tracing", lengthTraceFile);
  cmd.AddValue ("markTraceFile", "filename for n2 mark tracing", markTraceFile);
  cmd.AddValue ("marksFrequencyTraceFile", "filename for n2 mark frequency tracing", marksFrequencyTraceFile);
  cmd.AddValue ("queueDelayN0TraceFile", "filename for n0 queue delay tracing", queueDelayN0TraceFile);
  cmd.Parse (argc, argv);

  // Config File
  std::string configFile = dir + "config.txt";
  std::ofstream configOfStream;
  configOfStream.open (configFile.c_str (), std::ofstream::out);
  configOfStream << "n0TcpType: " << n0TcpType << "\n"
                 << "tcpStreams: " << tcpStreams << "\n"
                 << "bottleneckQueueType: " << queueType << "\n"
                 << "useEcn: " << useEcn << "\n"
                 << "linkDelay: " << linkDelay << "\n"
                 << "bottleneckDelay: " << bottleneckDelay << "\n"
                 << "bottleneckRate: " << bottleneckRate << "\n"
                 << "linkRate: " << linkRate << "\n"
                 << "stopTime: " << stopTime << "\n";
  configOfStream.close ();


  // Time oneWayDelay = baseRtt / 2;
  if (baseRtt != MilliSeconds (80))
    {
      linkDelay = baseRtt / 8;
      bottleneckDelay = baseRtt / 4;
    }

  bottleneckRateNumber =  DataRate (bottleneckRate).GetBitRate ();
  TypeId n0TcpTypeId;
  TypeId n1TcpTypeId;
  TypeId queueTypeId;

  // If UseEcn is true
  if (useEcn)
    {
      Config::SetDefault ("ns3::TcpSocketBase::UseEcn", StringValue ("On"));
    }

  // Get the TypeId () for the TCP Type selected on n0server and n4client
  if (n0TcpType == "reno")
    {
      n0TcpTypeId = TcpNewReno::GetTypeId ();
    }
  else if (n0TcpType == "bic")
    {
      n0TcpTypeId = TcpBic::GetTypeId ();
    }
  else if (n0TcpType == "dctcp")
    {
      n0TcpTypeId = TcpDctcp::GetTypeId ();
    }
  else if (n0TcpType == "cubic")
    {
      n0TcpTypeId = TcpCubic::GetTypeId ();
    }
  else
    {
      NS_FATAL_ERROR ("Fatal error:  tcp unsupported");
    }

  // Set the queueType
  if (queueType == "fqCodel")
    {
      queueTypeId = FqCoDelQueueDisc::GetTypeId ();
    }
  else if (queueType == "fqPie")
    {
      queueTypeId = FqPieQueueDisc::GetTypeId ();
    }
  else if (queueType == "fqCobalt")
    {
      queueTypeId = FqCobaltQueueDisc::GetTypeId ();
    }
  else if (queueType == "codel")
    {
      queueTypeId = CoDelQueueDisc::GetTypeId ();
    }
  else
    {
      NS_FATAL_ERROR ("Fatal error:  queueType unsupported");
    }
  if (useCeThreshold)
    {
      Config::SetDefault ("ns3::FqCoDelQueueDisc::CeThreshold", TimeValue (ceThreshold));
    }

  std::ofstream pingOfStream;
  pingOfStream.open (pingTraceFile.c_str (), std::ofstream::out);
  std::ofstream n0TcpRttOfStream;
  n0TcpRttOfStream.open (n0TcpRttTraceFile.c_str (), std::ofstream::out);
  std::ofstream n0TcpCwndOfStream;
  n0TcpCwndOfStream.open (n0TcpCwndTraceFile.c_str (), std::ofstream::out);
  std::ofstream n0TcpThroughputOfStream;
  n0TcpThroughputOfStream.open (n0TcpThroughputTraceFile.c_str (), std::ofstream::out);

  // Queue disc files
  std::ofstream dropOfStream;
  dropOfStream.open (dropTraceFile.c_str (), std::ofstream::out);
  std::ofstream markOfStream;
  markOfStream.open (markTraceFile.c_str (), std::ofstream::out);
  std::ofstream dropsFrequencyOfStream;
  dropsFrequencyOfStream.open (dropsFrequencyTraceFile.c_str (), std::ofstream::out);
  std::ofstream marksFrequencyOfStream;
  marksFrequencyOfStream.open (marksFrequencyTraceFile.c_str (), std::ofstream::out);
  std::ofstream lengthOfStream;
  lengthOfStream.open (lengthTraceFile.c_str (), std::ofstream::out);
  std::ofstream queueDelayN0OfStream;
  queueDelayN0OfStream.open (queueDelayN0TraceFile.c_str (), std::ofstream::out);
  std::ofstream linkUtilizationOfStream;
  linkUtilizationOfStream.open (linkUtilizationFile.c_str (), std::ofstream::out);

  ////////////////////////////////////////////////////////////
  // scenario setup                                         //
  ////////////////////////////////////////////////////////////
  Ptr<Node> pingServer = CreateObject<Node> ();
  Ptr<Node> n0Server = CreateObject<Node> ();
  Ptr<Node> n2 = CreateObject<Node> ();
  Ptr<Node> n3 = CreateObject<Node> ();
  Ptr<Node> pingClient = CreateObject<Node> ();
  Ptr<Node> n4Client = CreateObject<Node> ();

  // Device containers
  NetDeviceContainer pingServerDevices;
  NetDeviceContainer n0ServerDevices;
  NetDeviceContainer n2n3Devices;
  NetDeviceContainer pingClientDevices;
  NetDeviceContainer n4ClientDevices;

  PointToPointHelper p2p;
  p2p.SetQueue ("ns3::DropTailQueue", "MaxSize", QueueSizeValue (QueueSize ("3p")));
  p2p.SetDeviceAttribute ("DataRate", DataRateValue (DataRate (linkRate)));
  // Set linkDelay
  p2p.SetChannelAttribute ("Delay", TimeValue (linkDelay));
  pingServerDevices = p2p.Install (n2, pingServer);
  n0ServerDevices = p2p.Install (n2, n0Server);
  pingClientDevices = p2p.Install (n3, pingClient);
  n4ClientDevices = p2p.Install (n3, n4Client);
  // Set Bottleneck delay
  p2p.SetChannelAttribute ("Delay", TimeValue (bottleneckDelay));
  p2p.SetDeviceAttribute ("DataRate", DataRateValue (DataRate (bottleneckRate)));
  n2n3Devices = p2p.Install (n2, n3);

  InternetStackHelper stackHelper;
  stackHelper.InstallAll ();

  // Set the per-node TCP type here
  Ptr<TcpL4Protocol> proto;
  proto = n4Client->GetObject<TcpL4Protocol> ();
  proto->SetAttribute ("SocketType", TypeIdValue (n0TcpTypeId));
  proto = n0Server->GetObject<TcpL4Protocol> ();
  proto->SetAttribute ("SocketType", TypeIdValue (n0TcpTypeId));

  // Set Fq-CoDel on all the NetDevices except the bottleneck queue.
  TrafficControlHelper tchFq;
  tchFq.SetRootQueueDisc ("ns3::FqCoDelQueueDisc");
  // BQL-related
  tchFq.SetQueueLimits ("ns3::DynamicQueueLimits", "HoldTime", StringValue ("1ms"));
  tchFq.Install (pingServerDevices);
  tchFq.Install (n0ServerDevices);
  tchFq.Install (n2n3Devices.Get (1));  // n2 queue for bottleneck link
  tchFq.Install (pingClientDevices);
  tchFq.Install (n4ClientDevices);

  // Set the queue for bottleneck
  TrafficControlHelper tchN2;
  tchN2.SetRootQueueDisc (queueTypeId.GetName ());
  // TODO: Check if the BQL parameters are correct
  // tchN2.SetQueueLimits ("ns3::DynamicQueueLimits", "HoldTime", StringValue ("1000ms"));
  tchN2.Install (n2n3Devices.Get (0));

  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer pingServerIfaces = ipv4.Assign (pingServerDevices);
  ipv4.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer n0ServerIfaces = ipv4.Assign (n0ServerDevices);
  ipv4.SetBase ("172.16.1.0", "255.255.255.0");
  Ipv4InterfaceContainer n2n3Ifaces = ipv4.Assign (n2n3Devices);
  ipv4.SetBase ("192.168.1.0", "255.255.255.0");
  Ipv4InterfaceContainer pingClientIfaces = ipv4.Assign (pingClientDevices);
  ipv4.SetBase ("192.168.2.0", "255.255.255.0");
  Ipv4InterfaceContainer n4ClientIfaces = ipv4.Assign (n4ClientDevices);

  Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

  ////////////////////////////////////////////////////////////
  // application setup                                      //
  ////////////////////////////////////////////////////////////

  V4PingHelper pingHelper ("192.168.1.2");
  pingHelper.SetAttribute ("Interval", TimeValue (pingInterval));
  pingHelper.SetAttribute ("Size", UintegerValue (pingSize));
  ApplicationContainer pingContainer = pingHelper.Install (pingServer);
  Ptr<V4Ping> v4Ping = pingContainer.Get (0)->GetObject<V4Ping> ();
  v4Ping->TraceConnectWithoutContext ("Rtt", MakeBoundCallback (&TracePingRtt, &pingOfStream));
  pingContainer.Start (Seconds (1));
  pingContainer.Stop (stopTime - Seconds (1));

  BulkSendHelper tcp ("ns3::TcpSocketFactory", Address ());
  // set to large value:  e.g. 1000 Mb/s for 60 seconds = 7500000000 bytes
  tcp.SetAttribute ("MaxBytes", UintegerValue (7500000000));
  // Configure n4/n0 TCP client/server pair
  uint16_t n4Port = 5000;
  InetSocketAddress n0DestAddress (n4ClientIfaces.GetAddress (1), n4Port);
  tcp.SetAttribute ("Remote", AddressValue (n0DestAddress));

  for (uint32_t i = 0; i < tcpStreams; ++i)
    {
      ApplicationContainer n0App;
      n0App = tcp.Install (n0Server);
      n0App.Start (Seconds (5));
      n0App.Stop (stopTime - Seconds (5));
    }

  Address n4SinkAddress (InetSocketAddress (Ipv4Address::GetAny (), n4Port));
  PacketSinkHelper n4SinkHelper ("ns3::TcpSocketFactory", n4SinkAddress);
  ApplicationContainer n4SinkApp;
  n4SinkApp = n4SinkHelper.Install (n4Client);
  n4SinkApp.Start (Seconds (5));
  n4SinkApp.Stop (stopTime - MilliSeconds (4500));

  Ptr<PacketSink> psink = Ptr<PacketSink> (dynamic_cast<PacketSink *> (PeekPointer (n4SinkApp.Get(0))));
  psink->TraceConnectWithoutContext ("Rx", MakeCallback (TraceN0Rx));

  // Setup traces that can be hooked now
  Ptr<TrafficControlLayer> tc;
  Ptr<QueueDisc> qd;
  tc = n2n3Devices.Get (0)->GetNode ()->GetObject<TrafficControlLayer> ();
  qd = tc->GetRootQueueDiscOnDevice (n2n3Devices.Get (0));
  qd->TraceConnectWithoutContext ("Drop", MakeBoundCallback (&TraceDrop, &dropOfStream));
  qd->TraceConnectWithoutContext ("Mark", MakeBoundCallback (&TraceMark, &markOfStream));
  qd->TraceConnectWithoutContext ("PacketsInQueue", MakeBoundCallback (&TraceQueueLength, &lengthOfStream, bottleneckRate));
  qd->TraceConnectWithoutContext ("Dequeue", MakeBoundCallback (&PacketDequeue, &queueDelayN0OfStream));

  // Setup scheduled traces; TCP traces must be hooked after socket creation
  Simulator::Schedule (Seconds (5) + MilliSeconds (100), &ScheduleN0TcpRttTraceConnection, &n0TcpRttOfStream);
  Simulator::Schedule (Seconds (5) + MilliSeconds (100), &ScheduleN0TcpCwndTraceConnection, &n0TcpCwndOfStream);
  // Simulator::Schedule (Seconds (5) + MilliSeconds (100), &ScheduleN0PacketSinkConnection);
  Simulator::Schedule (throughputSamplingInterval, &TraceN0Throughput, &n0TcpThroughputOfStream, throughputSamplingInterval);

  Simulator::Schedule (marksSamplingInterval, &TraceMarksFrequency, &marksFrequencyOfStream, marksSamplingInterval);
  Simulator::Schedule (marksSamplingInterval, &TraceDropsFrequency, &dropsFrequencyOfStream, marksSamplingInterval);

  // Link Utilization
  Ptr<PointToPointNetDevice> ptx = n2n3Devices.Get (0)->GetObject<PointToPointNetDevice> ();
  ptx->TraceConnectWithoutContext ("PhyTxBegin", MakeBoundCallback (&TraceTx, &markOfStream));

  Simulator::Schedule (throughputSamplingInterval, &TraceLinkUtilization, &linkUtilizationOfStream, throughputSamplingInterval);
  if (enablePcap)
    {
      p2p.EnablePcapAll ("fq-comparison", false);
    }

  Simulator::Stop (stopTime);
  Simulator::Run ();

  pingOfStream.close ();
  n0TcpCwndOfStream.close ();
  n0TcpRttOfStream.close ();
  n0TcpThroughputOfStream.close ();
  dropOfStream.close ();
  markOfStream.close ();
  dropsFrequencyOfStream.close ();
  marksFrequencyOfStream.close ();
  lengthOfStream.close ();
  queueDelayN0OfStream.close ();
}
